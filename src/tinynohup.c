
// 20210227, for BSD.
// tinynohup.c 
// cc tinynohup.c -o tinynohup  

#include <stdio.h>
#include <stdlib.h> 
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <limits.h>
#include <err.h>


/*
 * tinynohup shall exit with one of the following values:
 * 126 - The utility was found but could not be invoked.
 * 127 - An error occurred in the nohup utility, or the utility could
 *       not be found.
 */

size_t strlcpy(char *restrict dst, const char *restrict src, size_t dstsize)
{
    int offset;

    /* duplicate the string up to dstsize */
    offset = 0;
        /* guard against silly dstsize values */
    if( dstsize > 0 )
    {
        while( *(src+offset) != '\0' )
        {
            /* bail if dstsize is met */
            if( offset==dstsize )
            {
                offset--;
                break;
            }

            /* duplicate the string */
            *(dst+offset) = *(src+offset);
            offset++;
        }
    }
    /* always remember to cap a created string! */
    *(dst+offset) = '\0';
    
    /* return the string length of src */
    while( *(src+offset) != '\0' )
        offset++;

    return(offset);
}




size_t strlcat(char *restrict dst, const char *restrict src, size_t dstsize)
{
    int d_len,s_len,offset,src_index;

    /* obtain initial sizes */
    d_len = strlen(dst);
    s_len = strlen(src);

    /* get the end of dst */
    offset = d_len;

    /* append src */
    src_index = 0;
    while( *(src+src_index) != '\0' )
    {
        *(dst+offset) = *(src+src_index);
        offset++;
        src_index++;
        /* don't copy more than dstsize characters
           minus one */
        if( offset==dstsize-1)
            break;
    }
    /* always cap the string! */
    *(dst+offset) = '\0';

    return( d_len+s_len );
}




#define EXIT_NOEXEC	126
#define EXIT_NOTFOUND	127
#define EXIT_MISC	127
#define	FILENAME	"tinynohup.out"







int dofile(void)
{
	int fd;
	const char *p;
	char path[PATH_MAX];

	p = FILENAME;
	if ((fd = open(p, O_RDWR|O_CREAT|O_APPEND, S_IRUSR|S_IWUSR)) >= 0)
		goto dupit;
	if ((p = getenv("HOME")) != NULL && *p != '\0' &&
	    (strlen(p) + strlen(FILENAME) + 1) < sizeof(path)) {
		(void)strlcpy(path, p, sizeof(path));
		(void)strlcat(path, "/", sizeof(path));
		(void)strlcat(path, FILENAME, sizeof(path));
		if ((fd = open(p = path, O_RDWR|O_CREAT|O_APPEND, S_IRUSR|S_IWUSR)) >= 0)
			goto dupit;
	}
	errx(EXIT_MISC, "can't open a nohup.out file");

    // the goto is very ugly right?
    dupit:
	(void)lseek(fd, (off_t)0, SEEK_END);
	if (dup2(fd, STDOUT_FILENO) == -1)
		err(EXIT_MISC, NULL);
	if (fd > STDERR_FILENO)
		(void)close(fd);

	(void) fprintf( stderr, "Sending output to %s\n", p);

}







int usage(void)
{
	(void)fprintf( stderr, "usage: nohup utility [arg ...]\n");
	exit(EXIT_MISC);
}





int main(int argc, char *argv[])
{
	int exit_status;

	if (argc < 2)
		usage();

        (void) fprintf( stderr, "Tinynohup: Start!\n" ); 

	if (isatty(STDOUT_FILENO) || errno == EBADF)      dofile();

	if ( (isatty(STDERR_FILENO) || errno == EBADF) &&
	    dup2(STDOUT_FILENO, STDERR_FILENO) == -1) 
	    {
		/* may have just closed stderr */
		(void)fprintf( stdin, "nohup: %s\n", strerror(errno));
		exit(EXIT_MISC);
	    }


	/*
	 * The tiny nohup utility shall take the standard action for all signals
	 * except that SIGHUP shall be ignored.
	 */
	(void)signal(SIGHUP, SIG_IGN);

	execvp(argv[1], &argv[1]);


	exit_status = (errno == ENOENT) ? EXIT_NOTFOUND : EXIT_NOEXEC;
	err(exit_status, "%s", argv[1]);

        printf( " Tinynohup : Bye !\n" ); 
	exit(0);
}


